const path = require('path')

module.exports = {
  roots: [path.resolve(__dirname, './src')],
  testEnvironment: 'jest-environment-jsdom-sixteen',
  displayName: 'tests',
  testMatch: ['**//__tests__/**/*.js', '**/?(*.)+(spec|test).[jt]s?(x)'],
  testURL: 'http://localhost',
  setupFilesAfterEnv: [path.resolve(__dirname, './setupTests.js')],
  collectCoverageFrom: [
    "**/*.{ts,tsx}",
    "!**/node_modules/**",
    "!**/vendor/**"
  ],
}
