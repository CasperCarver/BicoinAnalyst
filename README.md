# Bitcoin currencies and web analyser apps

Test projects for customer.

## Available Scripts

In the project directory, you can run:

Install dependencies

### `npm install`

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:8080/analysis](http://localhost:8080/analysis) to view `Web analyzer` in the browser.

Open [http://localhost:8080/currencies](http://localhost:8080/currencies) to view `Bitcoin Currency` in the browser.


The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
