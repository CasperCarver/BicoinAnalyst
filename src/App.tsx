import * as React from 'react';
import { BrowserRouter as Router, Redirect, Route, Switch } from 'react-router-dom';
import { ROUTES } from './utils/constants';

import Currencies from './pages/currencies/component';
import Analysis from './pages/analysis/component';

export const App = () => (
  <Router>
    <Switch>
      <Route exact path={ROUTES.MAIN} render={() => <Redirect to={ROUTES.ANALYSIS} />} />
      <Route path={ROUTES.CURRENCIES} component={Currencies} />
      <Route path={ROUTES.ANALYSIS} component={Analysis} />
    </Switch>
  </Router>
);
