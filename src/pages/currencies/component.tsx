import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useInterval from '../../hooks/useInterval';
import { ACTION_STATUSES, UPDATED_TIME, SORTING_METHODS } from '../../utils/constants';
import { getCurrencies } from '../../redux/models/currencies/actions';
import { FormattedMessage } from 'react-intl';
import { selectCurrentCurrencies, selectCurrentCurrenciesStatus } from '../../redux/models/currencies/selectors';
import Currency from './currency';

import { Container, Typography } from '@material-ui/core';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import SortByAlphaIcon from '@material-ui/icons/SortByAlpha';
import SortIcon from '@material-ui/icons/Sort';
import IconButton from '@material-ui/core/IconButton';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Alert from '@material-ui/lab/Alert';
import { formatDateTime } from '../../utils/dateFormatting';
import { useStyles } from './styles';

const Currencies = () => {
  const dispatch = useDispatch();

  const currentCurrencies = useSelector(selectCurrentCurrencies);
  const currentCurrenciesStatus = useSelector(selectCurrentCurrenciesStatus);
  const classes = useStyles();

  const [sort, setSort] = useState<string>(SORTING_METHODS.SORTING_BY_NAME.DESC);

  useInterval(() => {
    dispatch(getCurrencies.request());
  }, UPDATED_TIME);

  useEffect(() => {
    dispatch(getCurrencies.request());
  }, []);

  return (
    <Container maxWidth="sm">
      <Box m={2}>
        <h3 className="header-currencies">
          <FormattedMessage id="bitcoin_currencie" />
        </h3>
      </Box>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>
                <IconButton
                  size="small"
                  onClick={() =>
                    sort !== SORTING_METHODS.SORTING_BY_NAME.ASC
                      ? setSort(SORTING_METHODS.SORTING_BY_NAME.ASC)
                      : setSort(SORTING_METHODS.SORTING_BY_NAME.DESC)
                  }
                >
                  <SortByAlphaIcon fontSize="small" />
                </IconButton>
                <FormattedMessage id="currency" />
              </TableCell>
              <TableCell align="right">
                <IconButton
                  size="small"
                  onClick={() =>
                    sort !== SORTING_METHODS.SORTING_BY_RATE.DESC
                      ? setSort(SORTING_METHODS.SORTING_BY_RATE.DESC)
                      : setSort(SORTING_METHODS.SORTING_BY_RATE.ASC)
                  }
                >
                  <SortIcon fontSize="small" />
                </IconButton>
                <FormattedMessage id="rate" />
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(currentCurrenciesStatus === ACTION_STATUSES.FULFILLED || currentCurrencies.bpi) && (
              <Currency currencies={currentCurrencies.bpi} sortMethod={sort} />
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <Box m={2}>
        <Typography variant="body1" gutterBottom>
          <FormattedMessage id="last_update" />
          {(currentCurrenciesStatus === ACTION_STATUSES.FULFILLED || currentCurrencies.bpi) &&
            formatDateTime(currentCurrencies.time.updated)}
          <FiberManualRecordIcon
            color="primary"
            style={{ fontSize: '1rem' }}
            className={
              currentCurrenciesStatus === ACTION_STATUSES.PENDING
                ? classes.loadingBadgeActive
                : classes.loadingBadgeInactive
            }
          />
        </Typography>
      </Box>
      {currentCurrenciesStatus === ACTION_STATUSES.REJECTED && (
        <Alert severity="error">
          <FormattedMessage id="page_not_loaded" />
        </Alert>
      )}
    </Container>
  );
};

export default Currencies;
