import { makeStyles } from '@material-ui/core/styles';

import { THEME_SPACING } from '../../utils/constants';

export const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'left',
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(THEME_SPACING),
    },
  },
}));
