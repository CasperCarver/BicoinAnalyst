import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import PageTagsMetrics from './pageTagsMetrics';

import TextField from '@material-ui/core/TextField';
import { Container } from '@material-ui/core';
import Button from '@material-ui/core/Button';

import { isValidUrl } from '../../utils/urlValidate';
import { fetchXmlRequest } from '../../utils/getXmlRequest';
import { findUniqueTags, getMostPopularTag, getStructuredTags } from '../../utils/parseHtml';
import { getElementPosition, getDomPath } from '../../utils/findLongestPath';
import { selectHtmlString } from '../../redux/models/analysis/selectors';
import { METRICS_HEADERS } from '../../utils/constants';

const Analysis = () => {
  const [url, setUrl] = useState<string>('');
  const [isFieldDirty, setFieldDirty] = useState<boolean>(false);

  const [uniqueTags, setUniqueTags] = useState(null);
  const [popularTag, setPopularTag] = useState(null);
  const [usedTags, setUsedTags] = useState(null);
  const [path, setPath] = useState(null);

  const isUrlError = !isValidUrl(url) && isFieldDirty;
  const htmlString = useSelector(selectHtmlString);

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFieldDirty(true);
    setUrl(event.target.value);
  };

  useEffect(() => {
    if (htmlString) {
      const uniqueTag = findUniqueTags(htmlString);
      const mostPopularTag = getMostPopularTag(htmlString);
      const tags = getStructuredTags(htmlString);
      const popularElementPosition = getElementPosition(htmlString, mostPopularTag);
      const elementPath = getDomPath(htmlString, popularElementPosition);

      setUniqueTags(uniqueTag);
      setPopularTag(mostPopularTag);
      setUsedTags(tags);
      setPath(elementPath);
    }
  }, [htmlString]);

  const getInfo = () => {
    fetchXmlRequest(url);
  };

  return (
    <Container maxWidth="md">
      <div className="box-header">
        <h3 className="header-analysis">
          <FormattedMessage id="web_analyzer" />
        </h3>
      </div>

      <TextField
        id="standard-full-width"
        variant="outlined"
        fullWidth
        label={<FormattedMessage id="enter_url" />}
        value={url}
        name="url"
        error={isUrlError}
        onChange={onChange}
        helperText={isUrlError && <FormattedMessage id="incorrect_url" />}
        margin="normal"
      />
      <div className="custom-button">
        <Button
          onClick={() => {
            getInfo();
          }}
        >
          <FormattedMessage id="analysis" />
        </Button>
      </div>
      {uniqueTags && <PageTagsMetrics tags={uniqueTags} header={METRICS_HEADERS.UNIQUE_TAGS} />}
      {usedTags && <PageTagsMetrics tags={usedTags} header={METRICS_HEADERS.USED_TAGS} />}
      {popularTag && <PageTagsMetrics tags={[popularTag]} header={METRICS_HEADERS.POPULAR_TAG} />}
      {path && (
        <PageTagsMetrics
          tags={[
            {
              tagPath: path,
            },
          ]}
          header={METRICS_HEADERS.PATH}
        />
      )}
    </Container>
  );
};

export default Analysis;
