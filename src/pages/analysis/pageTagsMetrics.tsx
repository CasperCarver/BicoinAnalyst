import React from 'react';
import Box from '@material-ui/core/Box';
import { Typography } from '@material-ui/core';
import { FormattedMessage } from 'react-intl';
import Chip from '@material-ui/core/Chip';
import { useStyles } from './styles';

import { BOX_MARGIN } from '../../utils/constants';
import { formatString } from '../../utils/formatString';

export interface Props {
  tags: Array<Tag>;
  header: string;
}

const PageTagsMetrics: React.FC<Props> = ({ tags, header }) => {
  const classes = useStyles();
  return (
    <Box m={BOX_MARGIN}>
      <Typography variant="h5" gutterBottom>
        <FormattedMessage id={header} />
      </Typography>
      <div className={classes.root}>
        {tags.map(tag => (
          <Chip key={tag.tagName || tag.tagPath} variant="outlined" size="small" label={formatString(tag)} />
        ))}
      </div>
    </Box>
  );
};

export default PageTagsMetrics;
