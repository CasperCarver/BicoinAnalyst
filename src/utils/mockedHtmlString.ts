export const HTML_STRING = `<!DOCTYPE html>
<html lang="en">
<body>
<div class="event closed expand_block">
<div class="headline" id="test">
  <h3 class="h4 tPadding0 bPadding0 summary"> <span class="expand_section">Upcoming: W3C Workshop on
     Web</span> </h3>
      <div class="headline">
     <a> <p class="date"> <span class="dtstart published" title="2020-12-23T09:40:32Z"> 23 December 2020
    </span> | <a
      title="Archive: Upcoming: W3C Workshop on Wide Color Gamut and High Dynamic Range for the Web"
      href="https://www.w3.org/">Archive</a> </p></a>
      </div>
</div>
<div class="description expand_description" id="test">
  <p><a class="imageLink" href="https://www.w3.org/Graphics/Color/Workshop/"><img
        src="https://www.w3.org/Graphics/Color/Workshop/media/UCS-rec2020-ws.svg"
        alt="chromaticity diagram of wide color gamut" width="210" /></a> W3C announced today the <a
      href="https://www.w3.org/Graphics/Color/Workshop/">W3C Workshop on Wide Color Gamut (WCG) and
      High Dynamic Range (HDR) for the Web</a>, which is being organized as a virtual event in
    April-May 2021. </p>
  <p>The primary goal of the workshop is to bring together browser vendors</p>
  <p>The event will be organized as a combination of pre-recorded talks (expressions of interest are
    due <strong>30 January</strong> organized around three main themes: </p>
  <ul class="show_items">
    <li>Wide Color Gamut on the Web</li>
    <li>High Dynamic Range on the Web</li>
    <li>WCG and HDR Standardization Landscape</li>
  </ul>
  <p>The event is free and open to anyone with relevant perspectives on the topic to register for
    the event. For more information on the workshop, please see the <a
      href="https://www.w3.org/Graphics/Color/Workshop/">workshop details and submission
      instructions</a>. </p>
  <p>Deadline to <a href="https://www.w3.org/">submit a
      proposal for a talk</a> is <strong>30 January 2021</strong>, and registration will be open
    from mid January until April.</p>
</div>
</div>
</body>
</html>`;
