import { PATH_SEPARATOR, POSITIVE_SIBLINGS_NUMBER, INCREMENT_NEXT_TAG } from './constants';
import { getTags, parseElements } from './parseHtml';

interface ElementNode {
  parentNode: ParentNode;
  hasAttribute: (arg: string) => void;
  nodeName: string;
  id: string;
}

export const getDomPath = (htmlString: string, elementPosition: number) => {
  const allElements = parseElements(htmlString);
  let element: ElementNode | Element;
  element = allElements[elementPosition] as Element;
  const stack = [];

  while (element.parentNode !== null) {
    let siblingCount = 0;
    siblingCount = getSiblingCount(element as Element, siblingCount);

    if (element.hasAttribute('id') && element.id !== '') {
      stack.unshift(element.nodeName.toLowerCase() + '#' + element.id);
    } else if (siblingCount > POSITIVE_SIBLINGS_NUMBER) {
      stack.unshift(element.nodeName.toLowerCase());
    } else {
      stack.unshift(element.nodeName.toLowerCase());
    }
    element = element.parentNode as Element;
  }

  return stack.join(PATH_SEPARATOR.DEFAULT);
};

export const getSiblingCount = (element: Element, siblingCount: number) => {
  let parentsCount = siblingCount;
  for (let i = 0; i < element.parentNode.childNodes.length; i++) {
    const sibling = element.parentNode.childNodes[i];
    if (sibling.nodeName === element.nodeName) {
      parentsCount++;
    }
  }

  return parentsCount;
};

export const getElementPosition = (htmlString: string, tag: Tag) => {
  const tags = getTags(htmlString);
  const mostPopularTag = tag.tagName;
  const chainLength = {
    position: 0,
    length: 0,
  };
  for (let i = 0; i < tags.length; i++) {
    let localLength = 1;
    let localPosition = chainLength.position;
    if (tags[i] === mostPopularTag) {
      localPosition = i;
      let item = i;
      while (tags[item] === tags[item + INCREMENT_NEXT_TAG]) {
        localLength++;
        if (localLength > chainLength.length) {
          chainLength.length = localLength;
          chainLength.position = localPosition;
        }
        item++;
      }
    }
  }

  return chainLength.position;
};
