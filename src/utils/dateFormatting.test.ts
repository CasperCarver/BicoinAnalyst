import { formatDateTime } from './dateFormatting';

describe('formatDateTime', () => {
  it('should return hours and minutes', () => {
    const time = 'Jan 29, 2021 18:44:00';
    const expectedTime = '18:44';

    expect(formatDateTime(time)).toEqual(expectedTime);
  });
});
