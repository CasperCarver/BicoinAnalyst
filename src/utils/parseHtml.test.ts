import { getTags, getMostPopularTag, findUniqueTags, getStructuredTags } from './parseHtml';

const mockedHtmlString = '<div><div><a></a></div></div>';

describe('parseHtml', () => {
  describe('getUniqueTags', () => {
    it('should return array of unique tags', () => {
      const expectedResult = [
        {
          tagName: 'A',
        },
      ];

      expect(findUniqueTags(mockedHtmlString)).toEqual(expectedResult);
    });
  });

  describe('getUsedTags', () => {
    it('should return array of tags', () => {
      const expectedResult = [
        {
          tagCount: 2,
          tagName: 'DIV',
        },
        {
          tagCount: 1,
          tagName: 'A',
        },
      ];

      expect(getStructuredTags(mockedHtmlString)).toEqual(expectedResult);
    });
  });

  describe('getTags', () => {
    it('should return array of tags', () => {
      const expectedResult = ['DIV', 'DIV', 'A'];

      expect(getTags(mockedHtmlString)).toEqual(expectedResult);
    });
  });

  describe('getMostPopularTag', () => {
    it('should return array with most popular tag', () => {
      const expectedResult = {
        tagCount: 2,
        tagName: 'DIV',
      };

      expect(getMostPopularTag(mockedHtmlString)).toEqual(expectedResult);
    });
  });
});
