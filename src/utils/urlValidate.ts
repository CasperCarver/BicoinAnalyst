export const expression = /((?:(?:http(?:s|):\/\/)|(?:www\.))(?:[^,\s])+[^(?:\s\].,)])/gi;

export const isValidUrl = (string: string): boolean => {
  const urlRegExp = new RegExp(expression);

  return urlRegExp.test(string);
};
