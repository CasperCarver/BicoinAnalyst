export const ROUTES = {
  MAIN: '/',
  CURRENCIES: '/currencies',
  ANALYSIS: '/analysis',
};

export const ACTION_STATUSES = {
  PENDING: 'PENDING',
  FULFILLED: 'FULFILLED',
  REJECTED: 'REJECTED',
};

export const API_PATH = {
  BITCOIN_PRICE_API: 'https://api.coindesk.com/v1/bpi/currentprice.json',
  PROXY_SERVER_URL: 'https://thingproxy.freeboard.io/fetch/',
};

export const UPDATED_TIME = 5000;

export const STATUS_CODES = {
  OK: 200,
};

export const HTTP_METHODS = {
  GET: 'GET',
};

export const CONTENT_TYPE = {
  XML: 'application/xml',
};

export const XML_CLIENT_STATE = {
  DONE: 4,
};

export const BOX_MARGIN = 2;

export const SORTING_METHODS = {
  SORTING_BY_RATE: {
    DESC: 'SORTING_BY_RATE_DESC',
    ASC: 'SORTING_BY_RATE_ASC',
  },
  SORTING_BY_NAME: {
    ASC: 'SORTING_BY_NAME_ASC',
    DESC: 'SORTING_BY_NAME_DESC',
  },
};

export const METRICS_HEADERS = {
  USED_TAGS: 'used_tags',
  UNIQUE_TAGS: 'unique_tags',
  POPULAR_TAG: 'most_commonly_used_tag',
  PATH: 'path_most_commonly_used_tag',
};

export const PATH_SEPARATOR = {
  DEFAULT: ' > ',
};

export const EMPTY_DELAY = 0;

export const THEME_SPACING = 0.5;

export const COUNT_UNIQUE_TAG = 1;

export const POSITIVE_SIBLINGS_NUMBER = 1;

export const FIRST_TAG_INDEX = 0;

export const INCREMENT_NEXT_TAG = 1;
