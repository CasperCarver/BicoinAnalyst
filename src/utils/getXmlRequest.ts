import { store } from '../redux/configureStore';
import { setHtmlString } from '../redux/models/analysis/actions';
import { STATUS_CODES, XML_CLIENT_STATE, HTTP_METHODS, CONTENT_TYPE, API_PATH } from '../utils/constants';

export const fetchXmlRequest = (url: string) => {
  const xmlRequest = new XMLHttpRequest();
  xmlRequest.overrideMimeType(CONTENT_TYPE.XML);
  // Used proxy server to avoid cors policy
  xmlRequest.open(HTTP_METHODS.GET, `${API_PATH.PROXY_SERVER_URL}${url}`, true);

  xmlRequest.onreadystatechange = () => {
    if (xmlRequest.readyState === XML_CLIENT_STATE.DONE && xmlRequest.status === STATUS_CODES.OK) {
      store.dispatch(setHtmlString(xmlRequest.response));
    }
  };
  xmlRequest.send(null);
};
