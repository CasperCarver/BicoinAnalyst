export const formatString = (tag: Tag) => {
  return `${tag.tagName ? tag.tagName : tag.tagPath}${tag.tagCount ? `(${tag.tagCount})` : ''}`;
};
