import { HTML_STRING } from './mockedHtmlString';
import { getDomPath, getElementPosition } from './findLongestPath';

const mockedElementPosition = 6;
const mockedTagCount = 7;

describe('findLongestPath', () => {
  describe('getDomPath', () => {
    it('should return element path', () => {
      const expectedResult = 'div > div#test > div > p';
      expect(getDomPath(HTML_STRING, mockedElementPosition)).toEqual(expectedResult);
    });
  });

  describe('getElementPosition', () => {
    it('should return most used element position', () => {
      const expectedResult = 15;
      expect(
        getElementPosition(HTML_STRING, {
          tagCount: mockedTagCount,
          tagName: 'P',
        }),
      ).toEqual(expectedResult);
    });
  });
});
