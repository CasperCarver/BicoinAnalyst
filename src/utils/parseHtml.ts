import { COUNT_UNIQUE_TAG } from './constants';

export const findUniqueTags = (htmlString: string) => {
  return getStructuredTags(htmlString)
    .filter(tag => tag.tagCount === COUNT_UNIQUE_TAG)
    .map(tag => {
      return { tagName: tag.tagName };
    });
};

export const getTags = (htmlString: string) => {
  const allElements = parseElements(htmlString);
  const tags = [];
  for (let item = 0; item < allElements.length; item++) {
    tags.push(allElements[item].tagName);
  }

  return tags;
};

export const getMostPopularTag = (htmlString: string) => {
  const [firstTag] = getStructuredTags(htmlString).sort((a, b) => b.tagCount - a.tagCount);

  return firstTag;
};

export const parseElements = (htmlString: string) => {
  const wrapper = document.createElement('div');
  wrapper.innerHTML = htmlString;

  return wrapper.getElementsByTagName('*');
};

export const getStructuredTags = (htmlString: string) => {
  interface StructuredTagsObject {
    [key: string]: number;
  }

  const tags = getTags(htmlString);
  const structuredTagsObject: StructuredTagsObject = {};
  const structuredTags = [];

  tags.forEach(tag => {
    if (structuredTagsObject[tag]) {
      structuredTagsObject[tag]++;
    } else {
      structuredTagsObject[tag] = 1;
    }
  });

  let counter = 0;
  for (const tag in structuredTagsObject) {
    structuredTags[counter++] = {
      tagName: tag,
      tagCount: structuredTagsObject[tag],
    };
  }

  return structuredTags;
};
