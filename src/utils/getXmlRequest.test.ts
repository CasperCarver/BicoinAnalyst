import { fetchXmlRequest } from './getXmlRequest';
import { store } from '../redux/configureStore';

const mockedUrl = 'https://example.com/';
const callsNumber = 1;

jest.mock('../redux/configureStore.ts');
store.dispatch = jest.fn();

describe('getXmlRequest', () => {
  it('should fetch xhrrequest', () => {
    const xhrMock: Partial<XMLHttpRequest> = {
      open: jest.fn(),
      send: jest.fn(),
      setRequestHeader: jest.fn(),
      overrideMimeType: jest.fn(),
      readyState: 4,
      status: 200,
      response: 'Hello World!',
      onreadystatechange: jest.fn(),
    };

    jest.spyOn(window, 'XMLHttpRequest').mockImplementation(() => xhrMock as XMLHttpRequest);
    fetchXmlRequest(mockedUrl);

    expect(xhrMock.open).toBeCalledWith('GET', 'https://thingproxy.freeboard.io/fetch/https://example.com/', true);
    (xhrMock.onreadystatechange as EventListener)(new Event(''));
    expect(store.dispatch).toBeCalledTimes(callsNumber);
  });
});
