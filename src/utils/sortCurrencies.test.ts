import { SORTING_METHODS } from './constants';
import { sortCurrencies } from './sortCurrencies';

const mockingArray = [
  {
    code: 'USD',
    rate_float: 33488,
  },
  {
    code: 'EUR',
    rate_float: 20000,
  },
  {
    code: 'GBR',
    rate_float: 10000,
  },
];

describe('sortCurrencies', () => {
  it('should return sort array by rate from lower to higher', () => {
    const expectedValue = [
      {
        code: 'GBR',
        rate_float: 10000,
      },
      {
        code: 'EUR',
        rate_float: 20000,
      },
      {
        code: 'USD',
        rate_float: 33488,
      },
    ];

    expect(sortCurrencies(mockingArray, SORTING_METHODS.SORTING_BY_RATE.ASC)).toEqual(expectedValue);
  });

  it('should return sort array by rate from higher to lower', () => {
    const expectedValue = [
      {
        code: 'USD',
        rate_float: 33488,
      },

      {
        code: 'EUR',
        rate_float: 20000,
      },
      {
        code: 'GBR',
        rate_float: 10000,
      },
    ];

    expect(sortCurrencies(mockingArray, SORTING_METHODS.SORTING_BY_RATE.DESC)).toEqual(expectedValue);
  });

  it('should return sort array by name from lower to higher', () => {
    const expectedValue = [
      {
        code: 'EUR',
        rate_float: 20000,
      },
      {
        code: 'GBR',
        rate_float: 10000,
      },
      {
        code: 'USD',
        rate_float: 33488,
      },
    ];

    expect(sortCurrencies(mockingArray, SORTING_METHODS.SORTING_BY_NAME.ASC)).toEqual(expectedValue);
  });

  it('should return sort array by name from higher to lower', () => {
    const expectedValue = [
      {
        code: 'USD',
        rate_float: 33488,
      },
      {
        code: 'GBR',
        rate_float: 10000,
      },
      {
        code: 'EUR',
        rate_float: 20000,
      },
    ];

    expect(sortCurrencies(mockingArray, SORTING_METHODS.SORTING_BY_NAME.DESC)).toEqual(expectedValue);
  });

  it('should return default value', () => {
    expect(sortCurrencies(mockingArray, '')).toEqual(mockingArray);
  });
});
