import { expression, isValidUrl } from './urlValidate';

const FIRST_GROUP_INDEX = 0;

describe('isValidate', () => {
  describe('isCorrectUrl valid cases', () => {
    it('valid case with https', () => {
      expect(isValidUrl('https://www.example.com/subpath/?foo=bar')).toEqual(true);
    });

    it('valid case with http', () => {
      expect(isValidUrl('http://www.example.com/subpath/?foo=bar')).toEqual(true);
    });

    it('valid case without protocol', () => {
      expect(isValidUrl('www.example.com/subpath/?foo=bar')).toEqual(true);
    });
  });

  describe('isCorrectUrl invalid cases', () => {
    it('without protocol and www', () => {
      expect(isValidUrl('example.com/subpath/?foo=bar')).toEqual(false);
    });

    it('typo in protocol', () => {
      expect(isValidUrl('htttps://example.com/subpath/?foo=bar')).toEqual(false);
    });

    it('typo in www', () => {
      expect(isValidUrl('ww.example.com/subpath/?foo=bar')).toEqual(false);
    });

    it('email', () => {
      expect(isValidUrl('test@example.com')).toEqual(false);
    });

    it('Simple text', () => {
      expect(isValidUrl('Simle text')).toEqual(false);
    });
  });

  describe('expression matches are correct', () => {
    const expectedString = 'http://www.example.com';

    it('Expression dont match after space', () => {
      expect(new RegExp(expression).exec('http://www.exa mple.com')[FIRST_GROUP_INDEX]).toEqual('http://www.exa');
    });

    it('Expression dont match trailing bracket', () => {
      expect(new RegExp(expression).exec('http://www.example.com]')[FIRST_GROUP_INDEX]).toEqual(expectedString);
    });

    it('Expression dont match trailing dot', () => {
      expect(new RegExp(expression).exec('http://www.example.com.')[FIRST_GROUP_INDEX]).toEqual(expectedString);
    });

    it('Expression dont match trailing comma', () => {
      expect(new RegExp(expression).exec('http://www.example.com,')[FIRST_GROUP_INDEX]).toEqual(expectedString);
    });
  });
});
