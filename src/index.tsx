import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { App } from './App';
import { IntlProvider } from 'react-intl';
import en from './lang/en.json';
import './index.css';

import { Provider } from 'react-redux';
import { store } from './redux/configureStore';

const messages = {
  en: en,
};

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale={'en'} messages={messages.en}>
      <App />
    </IntlProvider>
  </Provider>,
  document.getElementById('output'),
);
