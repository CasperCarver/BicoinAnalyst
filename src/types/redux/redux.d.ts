interface CurrenciesState {
  currentCurrencies: CurrrenciesResponse;
  getCurrencies: {
    status: import('../../utils/constants').ACTION_STATUSES;
    error: Error;
  };
}

interface AnalysisState {
  currentHtmlString: string;
  uniqueTags: Array<Tag>;
  usedTags: Array<Tag>;
  mostPopularTag: Tag;
}

interface Tag {
  tagName?: string;
  tagCount?: number;
  tagPath?: string;
}

interface Redux {
  currencies: CurrenciesState;
  analysis: AnalysisState;
}
