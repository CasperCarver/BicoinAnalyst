import { applyMiddleware, createStore } from 'redux';

import { composeWithDevTools } from 'redux-devtools-extension';
import { createEpicMiddleware } from 'redux-observable';
import { rootEpic } from './rootEpic';
import { rootReducer } from './rootReducer';

const epicMiddleware = createEpicMiddleware();
const middleware = applyMiddleware(epicMiddleware);

const composeEnhancers = composeWithDevTools({});

const store = createStore(rootReducer, composeEnhancers(middleware));

epicMiddleware.run(rootEpic);

export { store };
