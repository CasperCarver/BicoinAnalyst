/* eslint-disable no-magic-numbers */
import { ACTION_STATUSES } from '../../../utils/constants';
import * as actions from './actions';

import { ActionType, createReducer } from 'typesafe-actions';

export const INITIAL_STATE: CurrenciesState = {
  currentCurrencies: {
    bpi: null,
    time: null,
    disclaimer: null,
    chartName: null,
  },
  getCurrencies: {
    status: null,
    error: null,
  },
};

const getCurrenciesRequestHandler = (state: CurrenciesState): CurrenciesState => ({
  ...state,
  getCurrencies: {
    status: ACTION_STATUSES.PENDING,
    error: null,
  },
});

const getCurrenciesSuccessHandler = (
  state: CurrenciesState,
  { payload }: ActionType<typeof actions.getCurrencies.success>,
): CurrenciesState => ({
  ...state,
  currentCurrencies: {
    ...payload,
    bpi: Object.entries(payload.bpi).reduce((array, item) => array.concat(item.slice(1)), []),
  },
  getCurrencies: {
    status: ACTION_STATUSES.FULFILLED,
    error: null,
  },
});

const getCurrenciesFailureHandler = (
  state: CurrenciesState,
  { payload: error }: ActionType<typeof actions.getCurrencies.failure>,
): CurrenciesState => ({
  ...state,
  getCurrencies: {
    status: ACTION_STATUSES.REJECTED,
    error,
  },
});

const currenciesReducer = createReducer(INITIAL_STATE)
  .handleAction(actions.getCurrencies.request, getCurrenciesRequestHandler)
  .handleAction(actions.getCurrencies.success, getCurrenciesSuccessHandler)
  .handleAction(actions.getCurrencies.failure, getCurrenciesFailureHandler);

export default currenciesReducer;
