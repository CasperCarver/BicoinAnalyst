import * as selectors from './selectors';

const INITIAL_STATE = {
  currencies: {
    currentCurrencies: {
      bpi: {},
    },
    getCurrencies: {
      status: '',
    },
  },
  analysis: {},
} as Redux;

describe('Currencied Selectors Tests', () => {
  describe('currenciesSelector', () => {
    it('should return currencies state', () => {
      expect(selectors.currenciesSelector(INITIAL_STATE)).toEqual(INITIAL_STATE.currencies);
    });
  });

  describe('selectCurrentCurrencies', () => {
    it('should return current currencies', () => {
      expect(selectors.selectCurrentCurrencies(INITIAL_STATE)).toEqual(INITIAL_STATE.currencies.currentCurrencies);
    });
  });

  describe('selectCurrentCurrenciesStatus', () => {
    it('should return current currencies status', () => {
      expect(selectors.selectCurrentCurrenciesStatus(INITIAL_STATE)).toEqual(
        INITIAL_STATE.currencies.getCurrencies.status,
      );
    });
  });
});
