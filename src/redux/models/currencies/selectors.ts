import { createSelector } from 'reselect';

export const currenciesSelector = (state: Redux) => state.currencies;

export const selectCurrentCurrencies = createSelector([currenciesSelector], currencies => currencies.currentCurrencies);

export const selectCurrentCurrenciesStatus = createSelector(
  [currenciesSelector],
  currencies => currencies.getCurrencies.status,
);
