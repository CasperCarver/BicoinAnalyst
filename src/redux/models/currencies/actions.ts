import { createAsyncAction } from 'typesafe-actions';

export const getCurrencies = createAsyncAction(
  'GET_CURRENCIES_REQUEST',
  'GET_CURRENCIES_SUCCESS',
  'GET_CURRENCIES_FAILURE',
)<[undefined, undefined], CurrrenciesResponse, Error>();
