import * as actions from './actions';
import { ACTION_STATUSES } from '../../../utils/constants';

import currenciesReducer, { INITIAL_STATE as state } from './reducers';

const mockedRate = 1;
const startIndex = 1;

describe('Currencies reducer tests', () => {
  describe('getCurrencies', () => {
    it('should set status of getCurrencies to pending', () => {
      const expectedState: CurrenciesState = {
        ...state,
        getCurrencies: {
          status: ACTION_STATUSES.PENDING,
          error: null,
        },
      };

      expect(currenciesReducer(state, actions.getCurrencies.request())).toEqual(expectedState);
    });

    it('should set status of getCurrencies to fulfilled', () => {
      const payload = {
        bpi: [
          {
            code: 'EUR',
            rate_float: mockedRate,
          },
        ],
      };
      const expectedState: CurrenciesState = {
        currentCurrencies: {
          ...payload,
          bpi: Object.entries(payload.bpi).reduce((array, item) => array.concat(item.slice(startIndex)), []),
        },

        getCurrencies: {
          status: ACTION_STATUSES.FULFILLED,
          error: null,
        },
      };

      expect(currenciesReducer(state, actions.getCurrencies.success(payload))).toEqual(expectedState);
    });

    it('should set an error for getcurrencied', () => {
      const error = {} as Error;
      const expectedState: CurrenciesState = {
        ...state,
        getCurrencies: {
          status: ACTION_STATUSES.REJECTED,
          error,
        },
      };

      expect(currenciesReducer(state, actions.getCurrencies.failure(error))).toEqual(expectedState);
    });
  });
});
