import * as actions from './actions';

import { Observable, of } from 'rxjs';
import { filter, mergeMap, map, catchError } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';

import { API_PATH } from '../../../utils/constants';
import { Epic } from 'redux-observable';
import { isActionOf } from 'typesafe-actions';
import { RootAction } from '../../rootAction';

export const getCurrenciesEpic: Epic<RootAction, RootAction> = action$ =>
  action$.pipe(
    filter(isActionOf(actions.getCurrencies.request)),
    mergeMap(
      (): Observable<RootAction> =>
        ajax({ url: API_PATH.BITCOIN_PRICE_API, method: 'GET' }).pipe(
          map(payload => actions.getCurrencies.success(payload.response)),
          catchError(error => of(actions.getCurrencies.failure(error.xhr.response))),
        ),
    ),
  );
