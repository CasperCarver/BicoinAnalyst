import * as actions from './actions';
import { findUniqueTags, getMostPopularTag, getStructuredTags } from '../../../utils/parseHtml';

import { ActionType, createReducer } from 'typesafe-actions';

export const INITIAL_STATE: AnalysisState = {
  currentHtmlString: null,
  uniqueTags: null,
  mostPopularTag: null,
  usedTags: null,
};

const setHtmlString = (state: AnalysisState, { payload }: ActionType<typeof actions.setHtmlString>): AnalysisState => ({
  ...state,
  currentHtmlString: payload,
  uniqueTags: findUniqueTags(payload),
  mostPopularTag: getMostPopularTag(payload),
  usedTags: getStructuredTags(payload),
});

const analysisReducer = createReducer(INITIAL_STATE).handleAction(actions.setHtmlString, setHtmlString);

export default analysisReducer;
