import { createAction } from 'typesafe-actions';

export const setHtmlString = createAction('PARSE_HTML_TO_STRING')<string>();

export const setUniqueTags = createAction('PARSE_UNIQUE_TAGS')<Array<string | number>>();
