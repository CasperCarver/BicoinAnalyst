import { createSelector } from 'reselect';

export const analysisSelector = (state: Redux) => state.analysis;

export const selectHtmlString = createSelector([analysisSelector], analysis => analysis.currentHtmlString);

export const selectUniqueTags = createSelector([analysisSelector], analysis => analysis.uniqueTags);
