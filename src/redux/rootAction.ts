import { ActionType } from 'typesafe-actions';
import * as currencies from './models/currencies/actions';
import * as analysis from './models/analysis/actions';
import { RouterAction } from 'connected-react-router';

const actions = {
  currencies,
  analysis,
};

export type RootAction = ActionType<typeof actions> | RouterAction;
