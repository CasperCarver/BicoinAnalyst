import { combineEpics } from 'redux-observable';

import * as getCurrenciesEpic from './models/currencies/epics';

export const rootEpic = combineEpics(...Object.values(getCurrenciesEpic));
