import { combineReducers } from 'redux';

import currenciesReducer from './models/currencies/reducers';
import analysisReducer from './models/analysis/reducers';

export const rootReducer = combineReducers({
  currencies: currenciesReducer,
  analysis: analysisReducer,
});
