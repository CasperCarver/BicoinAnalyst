import { renderHook } from '@testing-library/react-hooks';
import useInterval from './useInterval';

let callback: () => void;
const firstCall = 1;
const mockedDelay = 200;
const jestAdvanceTime = 199;
const callsNumberNextTimer = 2;
const mockedAdvanceNextTimers = 3;
const callsNumberMockedTimers = 5;

beforeEach(() => {
  callback = jest.fn();
});

beforeAll(() => {
  jest.useFakeTimers();
});

afterEach(() => {
  jest.clearAllTimers();
});

afterAll(() => {
  jest.useRealTimers();
});

describe('useInterval tests', () => {
  it('should repeatedly calls provided callback with a fixed time delay between each call', () => {
    renderHook(() => useInterval(callback, mockedDelay));
    expect(callback).not.toHaveBeenCalled();

    // fast-forward time until 1s before it should be executed
    jest.advanceTimersByTime(jestAdvanceTime);
    expect(callback).not.toHaveBeenCalled();

    // fast-forward until 1st call should be executed
    jest.advanceTimersByTime(firstCall);
    expect(callback).toHaveBeenCalledTimes(firstCall);

    // fast-forward until next timer should be executed
    jest.advanceTimersToNextTimer();
    expect(callback).toHaveBeenCalledTimes(callsNumberNextTimer);

    // fast-forward until 3 more timers should be executed
    jest.advanceTimersToNextTimer(mockedAdvanceNextTimers);
    expect(callback).toHaveBeenCalledTimes(callsNumberMockedTimers);
  });

  it('should init hook without delay', () => {
    const { result } = renderHook(() => useInterval(callback, null));

    expect(result.current).toBeUndefined();
    // if null delay provided, it's assumed as no delay
    expect(setInterval).toHaveBeenCalled();
  });
});
